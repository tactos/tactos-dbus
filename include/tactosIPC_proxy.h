#pragma once
#include <string>
#include <tuple>
#include <vector>
#include <glibmm.h>
#include <giomm.h>
#include "tactosIPC_common.h"

namespace org {
namespace tactos {

class ipcProxy : public Glib::ObjectBase {
public:
    static void createForBus(Gio::DBus::BusType busType,
                             Gio::DBus::ProxyFlags proxyFlags,
                             const std::string &name,
                             const std::string &objectPath,
                             const Gio::SlotAsyncReady &slot,
                             const Glib::RefPtr<Gio::Cancellable> &cancellable = {});

    static Glib::RefPtr<ipcProxy> createForBusFinish (const Glib::RefPtr<Gio::AsyncResult> &result);

    static Glib::RefPtr<ipcProxy> createForBus_sync(
        Gio::DBus::BusType busType,
        Gio::DBus::ProxyFlags proxyFlags,
        const std::string &name,
        const std::string &objectPath,
        const Glib::RefPtr<Gio::Cancellable> &cancellable = {});

    Glib::RefPtr<Gio::DBus::Proxy> dbusProxy() const { return m_proxy; }

    void publisherJoin(
        gint32 pid,
        const Gio::SlotAsyncReady &slot,
        const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void publisherJoin_finish (
        bool &result,
        const Glib::RefPtr<Gio::AsyncResult> &res);

    bool
    publisherJoin_sync(
        gint32 pid,const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void publisherLeave(
        const Gio::SlotAsyncReady &slot,
        const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void publisherLeave_finish (
        const Glib::RefPtr<Gio::AsyncResult> &res);

    void
    publisherLeave_sync(
const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void readerJoin(
        const Gio::SlotAsyncReady &slot,
        const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void readerJoin_finish (
        const Glib::RefPtr<Gio::AsyncResult> &res);

    void
    readerJoin_sync(
const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void readerLeave(
        const Gio::SlotAsyncReady &slot,
        const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    void readerLeave_finish (
        const Glib::RefPtr<Gio::AsyncResult> &res);

    void
    readerLeave_sync(
const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
        int timeout_msec = -1);

    std::tuple<guchar,guchar> Size_get(bool *ok = nullptr);
    void Size_set(const std::tuple<guchar,guchar> &, const Gio::SlotAsyncReady &);
    void Size_set_finish(const Glib::RefPtr<Gio::AsyncResult> &);
    void Size_set_sync(const std::tuple<guchar,guchar> &);
    sigc::signal<void> &Size_changed() {
        return m_Size_changed;
    }

    std::vector<bool> Matrix_get(bool *ok = nullptr);
    void Matrix_set(const std::vector<bool> &, const Gio::SlotAsyncReady &);
    void Matrix_set_finish(const Glib::RefPtr<Gio::AsyncResult> &);
    void Matrix_set_sync(const std::vector<bool> &);
    sigc::signal<void> &Matrix_changed() {
        return m_Matrix_changed;
    }

    bool Publisher_get(bool *ok = nullptr);
    sigc::signal<void> &Publisher_changed() {
        return m_Publisher_changed;
    }

    guchar Reader_get(bool *ok = nullptr);
    sigc::signal<void> &Reader_changed() {
        return m_Reader_changed;
    }


    void reference() const override {}
    void unreference() const override {}

protected:
    Glib::RefPtr<Gio::DBus::Proxy> m_proxy;

private:
    ipcProxy(const Glib::RefPtr<Gio::DBus::Proxy> &proxy);

    void handle_signal(const Glib::ustring &sender_name,
                       const Glib::ustring &signal_name,
                       const Glib::VariantContainerBase &parameters);

    void handle_properties_changed(const Gio::DBus::Proxy::MapChangedProperties &changed_properties,
                                   const std::vector<Glib::ustring> &invalidated_properties);

    sigc::signal<void> m_Size_changed;
    sigc::signal<void> m_Matrix_changed;
    sigc::signal<void> m_Publisher_changed;
    sigc::signal<void> m_Reader_changed;
};

}// tactos
}// org

