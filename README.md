# Tactos-dbus

Basic library to expose a service on dbus on `org/tactos/ipc`. This project contain a python service and the C++ client interface library created with [gdbus-codegen-glibmm](https://github.com/Pelagicore/gdbus-codegen-glibmm).

Used by [tactos-core-color](https://gitlab.com/tactos/tactos-core-color), [tactos-MIT4](https://gitlab.com/tactos/tactos-MIT4) and [tactos-term](https://gitlab.com/tactos/tactos-term)

## Building

This library is used as subproject and automatically compiled by other projects.

### Dependencies

 - meson (build)
 - giomm-2.4


### Compilation
It uses [meson](https://mesonbuild.com/) as build system and can be compiled with following instructions :


```bash
meson --buildtype=release builddir
cd builddir
ninja
```

## Documentation

see [DBus_interface](DBus_interface.md) for interface explaination.

## Contact

This library is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/).
