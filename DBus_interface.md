# DBus Interfaces

Instead of having a huge software to perform all actions we need, choice have been
done to develop **Tactos** with a modularity approach. As a result, **Tactos** is 
a set of software wrking together. They need to exchange data together and to do 
so, [DBus](https://www.freedesktop.org/wiki/Software/dbus/) have been chosen because
it has **modularity**, **cross-languages implementations**, good **linux integration**
and it's fast enough for our needs.

![Tactos IPC schematic](tactos_dbus_ipc.svg)

## Tactos-IPC interface 
IPC interface is implemented in the ``tactos-dbus`` project. This project is DBus activable
and in charge of managing communication between the core that publish the Matrix
values and the Reader that get the Matrix value and send it to device. 
It implement the following interface : 

- **Properties :**
	- **Size (Tuple[u8, u8])** [RW]: Width and high of the output matrix to write on Braille device.
	- **Matrix (array[bool])** [RW] : Value of the output matrix.
	- **Readers (u8)** [R] : number of reader registered
	- **Publisher (bool)** [R] : presence of a publisher. We allow only one publisher
	at a time.
- **Methods :**
	- **readerJoin** : Register a new reader. Because reader are not really important,
	reader are anonymous.
	- **readerLeave** : deregister a reader.
	- **pulblisherjoin(pid: int)->bool** : try to register as publisher. If a publisher
	 already exist,	return **False** except if the pid registered is no more active.
	 Register and return **True** otherwise.
	 - **publisherLeave** : deregister the current publisher.
	 
See [interface.xml](python/DBus_interface.xml) for more details.