#!/bin/bash
# set path of tactos-dbus executable in the service file.
# the path configured dont care about $DESTDIR variable
SERVICE_SOURCE=$2
echo $MESON_BUILD_ROOT >> test
echo  $MESON_SOURCE_ROOT/$SERVICE_SOURCE/python/org.tactos.ipc.service  >> source
sed  "s|__PATH__|$1|g" $MESON_SOURCE_ROOT/$SERVICE_SOURCE/python/org.tactos.ipc.service > $MESON_BUILD_ROOT/org.tactos.ipc.service
mkdir -p $DESTDIR/usr/share/dbus-1/services/
cp $MESON_BUILD_ROOT/org.tactos.ipc.service $DESTDIR/usr/share/dbus-1/services/
