#! /usr/bin/python3
from gi.repository import GLib
from pydbus import SessionBus

from pydbus.generic import signal
import numpy as np
import pkg_resources
from psutil import pid_exists

from logging import warning, error
from typing import Optional, Tuple, List

CONST_DBUS_ADDR = "org.tactos.ipc"
CONST_XML_FILE = "DBus_interface.xml"


class TactosService(object):
    """
    @brief implementation on DBus interface for tactos IPC
    """
    dbus = [pkg_resources.resource_string(__name__, CONST_XML_FILE).decode("utf-8")]

    def __init__(self):
        self._size: Tuple[int, int] = (4, 4)  # WIll be considered as unsigned byte
        self._matrix = np.zeros(self._size[0]*self._size[1], dtype=bool)
        self._publisher = False
        self._reader = 0 # will be considered as unsigned byte
        self._pid: Optional[int] = None

    @property
    def Size(self) -> Tuple[int, int]:
        return self._size

    @Size.setter
    def Size(self, value: Tuple[int, int]) -> None:
        """
        @brief Number of lines and colums of the grid.
        :param value: Tuple(lines, columns)
        :type value: DBus type : (yy)
        """
        self._size = value
        self.PropertiesChanged(CONST_DBUS_ADDR, {"Size": self.Size}, [])

    @property
    def Matrix(self) -> List[bool]:
        return self._matrix

    @Matrix.setter
    def Matrix(self, value: List[bool]) -> None:
        """
        @brief values of each pin of the grid
        :param value: array(pin1, pin2, pin3...)
        :type value: DBus type : ab
        """
        self._matrix = value
        self.PropertiesChanged(CONST_DBUS_ADDR, {"Matrix": self.Matrix}, [])

    @property
    def Publisher(self) -> bool:
        """
        @brief read-only property to tell if some program registered to publish values
        :returns: Bool. True if publisher is present, False if not.
        """
        return self._publisher

    @property
    def Reader(self) -> int:
        """
        @brief read-only property. Store the number of of programms registered to get matrix values
        :returns: unsigned Byte. (DBus type : y)
        """
        return self._reader

    def readerJoin(self) -> None:
        """
        @brief register new reader
        """
        self._reader += 1
        self.PropertiesChanged(CONST_DBUS_ADDR, {"Reader": self.Reader}, [])

    def readerLeave(self) -> None:
        """
        @brief deregister a reader
        """
        if(self._reader > 0):
            self._reader -= 1
            self.PropertiesChanged(CONST_DBUS_ADDR, {"Reader": self.Reader}, [])

        else:
            warning("number of reader should not be less than 0")

    def publisherJoin(self, pid: int) -> bool:
        """
        @brief Try to register a new publisher. It can be only one publisher at a time so that if publisher is true, check if registered publishher is still running. If so return **False**. Otherwise register and return True.
        :param pid: PID of the new publiseher to register
        :type pid: int
        :returns: True if success, False otherwise.
        """
        if self._publisher:
            if pid_exists(self._pid):
                error("A publisher already exixt with pid : {}".format(self._pid))
                return False
        self._pid = pid
        self._publisher = True
        self.PropertiesChanged(CONST_DBUS_ADDR, {"Publisher": self.Publisher}, [])
        return True

    def publisherLeave(self) -> None:
        """
        @brief deregister existing publisher.
        """
        self._publisher = False
        self._pid = None
        self.PropertiesChanged(CONST_DBUS_ADDR, {"Publisher": self.Publisher}, [])

    PropertiesChanged = signal()


if __name__ == "__main__":
    bus = SessionBus()
    bus.publish(CONST_DBUS_ADDR, TactosService())
    loop = GLib.MainLoop()
    loop.run()

